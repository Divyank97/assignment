const express = require("express");
const bodyParser = require("body-parser");

const mongoose = require("./db");
const routes = require("./routes");

const PORT = 1234;
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use("/api", routes);

app.listen(PORT, () => {
  console.log("Server is listening on port: 1234");
});
