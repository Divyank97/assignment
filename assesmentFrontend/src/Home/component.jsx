import React from "react";
import ReactLoading from "react-loading";
import {
  H1,
  Wrapper,
  TextContainer,
  InputBox,
  Input,
  Button,
  Anchor
} from "./styled";

type Props = {
  loading: boolean,
  generator: boolean,
  url: string,
  shortUrl: string,
  onChange: () => void,
  onSubmit: () => void
};

const Home = ({
  loading,
  generator,
  url,
  shortUrl,
  onChange,
  onSubmit
}: Props) => (
  <Wrapper>
    <TextContainer>
      <H1 fontSize="36px">Short Url Generator</H1>
    </TextContainer>
    <InputBox>
      <H1 fontSize="18px"> Enter Your Url</H1>
      <Input name="url" type="text" onChange={onChange} value={url} />
    </InputBox>
    <InputBox>
      <Button onClick={onSubmit}>Generate Url</Button>
    </InputBox>
    {loading && (
      <InputBox>
        <ReactLoading
          type="spinningBubbles"
          color="#fafafa"
          height={50}
          width={50}
        />
      </InputBox>
    )}
    {generator && (
      <InputBox>
        <H1 fontSize="18px"> Generated Short Link</H1>
        <Anchor href={`http://localhost:1234/api/links/${shortUrl}`}>
          {" "}
          http://localhost:1234/api/links/{shortUrl}
        </Anchor>
      </InputBox>
    )}
  </Wrapper>
);

export default Home;
