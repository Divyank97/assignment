const express = require("express");
const controller = require("../controllers/shortLink");

const router = express.Router();

router.get("/:id", controller.findShortLink);

router.post("/", controller.createShortLink);

module.exports = router;
