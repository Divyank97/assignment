const express = require("express");
const routes = require("./shortLink");

const router = express.Router();

router.use("/links", routes);

module.exports = router;
